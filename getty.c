/*
 * UNG's Not GNU
 *
 * Copyright (c) 2020 Jakob Kaivo <jkk@ung.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _POSIX_C_SOURCE 200809L
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef __linux__
#include <sys/ioctl.h>
#define DEFAULT_GETTY_TYPE "vt100"
#endif

#ifndef DEFAULT_GETTY_DEVICE
#define DEFAULT_GETTY_DEVICE "/dev/console"
#endif

#ifndef DEFAULT_GETTY_TYPE
#define DEFAULT_GETTY_TYPE "ecma-48"
#endif

static const char *fallbacks[] = {
	"/sys/bin/login",
	"/bin/login",
};
static const size_t nfallbacks = sizeof(fallbacks) / sizeof(fallbacks[0]);

int main(int argc, char *argv[])
{
	char *command = fallbacks[0];
	char *device = DEFAULT_GETTY_DEVICE;
	char *type = DEFAULT_GETTY_TYPE;

	int c;
	while ((c = getopt(argc, argv, "e:d:T:")) != -1) {
		switch (c) {
		case 'e':
			command = optarg;
			break;

		case 'd':
			device = optarg;
			break;

		case 'T':
			type = optarg;
			break;

		default:
			return 1;
		}
	}

	if (argc > optind) {
		fprintf(stderr, "getty: unexepcted operands\n");
		return 1;
	}

	setsid();

	int fd = open(device, O_RDWR | O_CLOEXEC | O_TTY_INIT, 0600);
	if (fd == -1 || !isatty(fd)) {
		fprintf(stderr, "getty: %s: %s\n", device, strerror(errno));
		return 1;
	}

	#ifdef __linux__
	if (ioctl (fd, TIOCSCTTY, (void *) 1) == -1) {
		perror("getty: ioctl");
		return 1;
	}
	#endif

	dup2(fd, STDIN_FILENO);
	dup2(fd, STDOUT_FILENO);
	dup2(fd, STDERR_FILENO);

	setenv("TERM", type, 1);
	/* TODO: wordexp() */
	char *args[] = { command, NULL };

	execv(args[0], args);
	if (errno == ENOENT) {
		for (size_t i = 0; i < nfallbacks; i++) {
			char *fallback[] = { fallbacks[i], NULL };
			execv(fallback[0], fallback);
		}
	}

	return 1;
}
